# sh/ksh initialization 
# loksh (linux ported openbsd ksh).

. /etc/ksh.kshrc

# define and determine the values of the system environment
set -o emacs
set -o noclobber

# enable line wrapping in terminal
tput smam

# clear the screen (if possible) and redraw the current line
bind -m '^L'=clear'^J'

# searching
bind '^F'=search-history
bind '^G'=abort

# pc-style ctl-delete
bind '^[[M'=delete-word-forward

# environment variables
unset  ENV
export PS1='\033[32m\w\033[36m >\033[00m '
export BROWSER=firefox-esr
export EDITOR=nano
export HISTCONTROL=ignoredups:ignorespace
export HISTFILE=~/.ksh_history
export HISTSIZE=20736
export PAGER=less
export LESS='-CdeiM'
export LESSSECURE=1
export LESSHISTFILE=-
export SHFM_OPENER='${HOME}/bin/opener'
export VISUAL='${EDITOR}'


# enable color support of ls
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval '$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)'
fi

# ksh tab completions
set -A complete_sudo_1 -- $(ls /usr/bin/) -- $(ls /bin/) -- $(ls /sbin/) -- $(ls /usr/sbin/)
set -A complete_apt_1 -- update upgrade full-upgrade dist-upgrade install reinstall remove purge autoremove satisfy search show list

if [ -n "$(command -v pass)" ]; then
	PASS_LIST=$(find "$HOME/.password-store" -type f -name '*.gpg' | sed 's/^.*\.password-store\///' | sed 's/\.gpg$//g')
	set -A complete_pass_1 -- $PASS_LIST generate edit insert git otp
	set -A complete_pass_2 -- $PASS_LIST push pull
fi

# aliases
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'

alias ls='ls --color=auto'
alias ll='ls -l'
alias la='ls -lA'
alias cp='cp -iv'
alias mv='mv -iv'
alias rm='safe-rm -vi'
alias grep='grep --color=auto'
alias pgrep='pgrep -q exec >/dev/null 2>&1'
alias mkdir='mkdir -pv'
alias mime='file -Lb --mime-type'
alias df='df -h'
alias du='du -ch'
alias sudo='sue'
alias tree="tree -C"

alias yt-dlp='yt-dlp --restrict-filenames'
alias yt-mp3='yt-dlp --config-location ~/.config/yt-mp3/config'

alias aps='apt search'
alias api='sudo apt install'
alias apu='sudo apt update'
alias apdu='sudo apt dist-upgrade'
alias app='sudo apt purge'
alias apa='sudo apt autoremove'
alias appc="sudo apt purge '~c'"
alias apsh='apt show'
alias gitupdate="git add . && git commit -m update && git push -u origin main"
