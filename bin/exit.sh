#!/usr/bin/env oksh
set -x

RET=$(echo -e "Logout\nReboot\nPoweroff\nCancel" | dmenu -p "Exit Menu")

case $RET in
	Logout) pkill -U "$USER" ;;
	Reboot) sup shutdown -r now ;;
	Poweroff) sup shutdown -h now ;;
	*) ;;
esac
