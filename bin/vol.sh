#!/bin/bash

[ $(amixer get Master | awk -F'[][]' 'END{ print $6 }') = off ] && echo 婢 && exit

vol="$(amixer get Master | awk -F'[]%[]' 'END{ print $2 }')"
mute="$(amixer get Master | awk -F'[][]' 'END{ print $6 }')"

if [ "$vol" -gt "90" ]; then
	icon="墳"
elif [ "$vol" -gt "60" ]; then
	icon="奔"
elif [ "$vol" -gt "0" ]; then
	icon="奄"
else
        echo "婢" && exit
fi

echo "$icon"
