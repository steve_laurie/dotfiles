#!/bin/sh

# this is an upgrade of my old ytsearch script, which was created so that I could search youtube quickly without searching with !yt on duckduckgo.
# this one is browser-agnostic and uses invidious instead of youtube.

instancelink="https://iteroni.com"

query="$(printf '' | dmenu -p 'Invidious: ')" || exit

if [ "$query" ]
then
	xdg-open "${instancelink}/search?q=$query"
else
	xdg-open "${instancelink}"
fi
