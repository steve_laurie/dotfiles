#!/usr/bin/env bash

a=$(ip r | awk '/^default/{print $5}')
b=$'wlan0\nusb0'
c="%printf '$b'"
d=$(iw dev | grep ssid | awk '{print $2}')

if      [[ "$a" = "wlan0" ]] ; then
        echo "$d"

elif    [[ "$a" = "usb0" ]] ; then
        echo "a"

else
        echo " "
fi


