#!/bin/sh

temp="$(sensors|awk '/^Core/{sum+=$3;lines+=1}END{print int (sum/lines)}')"
echo "/usr/share/icons/Papirus-Dark/symbolic/devices/cpu-symbolic.svg"
printf " %s°C   " "$temp"

